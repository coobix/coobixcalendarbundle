<?php

namespace Coobix\CalendarBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EventType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id')
            ->add('cid')
            
            ->add('title')
            ->add('start', 'datetime', array(
                'widget' => 'single_text',
                'format' => "yyyy-MM-dd HH:mm:ss",
                ))//yyyy-mm-ddthh mm ssz
                          //  "2017-10-18T01:00:00-03:00"
            ->add('end', 'datetime', array(
                'widget' => 'single_text',
                'format' => "yyyy-MM-dd HH:mm:ss",
                ))
            ->add('loc')
            ->add('notes')
            ->add('url')
            ->add('ad')
            ->add('rem')
            ->add('n')
        

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Coobix\CalendarBundle\Entity\Event',
            'csrf_protection' => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'event';
    }
}
