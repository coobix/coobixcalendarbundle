<?php

namespace Coobix\CalendarBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Coobix\CalendarBundle\Form\EventType as BaseType;

class EventEditType extends BaseType {}
