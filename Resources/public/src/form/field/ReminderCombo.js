/**
 * @class Ext.calendar.form.field.ReminderCombo
 * @extends Ext.form.ComboBox
 * <p>A custom combo used for choosing a reminder setting for an event.</p>
 * <p>This is pretty much a standard combo that is simply pre-configured for the options needed by the
 * calendar components. The default configs are as follows:<pre><code>
    width: 200,
    fieldLabel: 'Reminder',
    queryMode: 'local',
    triggerAction: 'all',
    forceSelection: true,
    displayField: 'desc',
    valueField: 'value'
</code></pre>
 * @constructor
 * @param {Object} config The config object
 */
Ext.define('Ext.calendar.form.field.ReminderCombo', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.reminderfield',

    fieldLabel: 'Recordatorio',
    queryMode: 'local',
    triggerAction: 'all',
    forceSelection: true,
    displayField: 'desc',
    valueField: 'value',

    // private
    initComponent: function() {
        this.store = this.store || new Ext.data.ArrayStore({
            fields: ['value', 'desc'],
            idIndex: 0,
            data: [
            ['', 'Ninguno'],
            ['0', 'A la hora del comienzo'],
            ['5', '5 minutos antes del comienzo'],
            ['15', '15 minutos antes del comienzo'],
            ['30', '30 minutos antes del comienzo'],
            ['60', '1 hora antes del comienzo'],
            ['90', '1.5 horas antes del comienzo'],
            ['120', '2 horas antes del comienzo'],
            ['180', '3 horas antes del comienzo'],
            ['360', '6 horas antes del comienzo'],
            ['720', '12 horas antes del comienzo'],
            ['1440', '1 día antes del comienzo'],
            ['2880', '2 días antes del comienzo'],
            ['4320', '3 días antes del comienzo'],
            ['5760', '4 días antes del comienzo'],
            ['7200', '5 días antes del comienzo'],
            ['10080', '1 semana antes del comienzo'],
            ['20160', '2 semanas antes del comienzo']
            ]
        });

        this.callParent();
    },

    // inherited docs
    initValue: function() {
        if (this.value !== undefined) {
            this.setValue(this.value);
        }
        else {
            this.setValue('');
        }
        this.originalValue = this.getValue();
    }
});
