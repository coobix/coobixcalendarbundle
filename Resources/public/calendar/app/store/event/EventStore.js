Ext.define('Calendar.store.event.EventStore', {
    extend: 'AppDesktop.baseclass.store.BaseStore',
    model: 'Calendar.model.event.EventModel',
    autoLoad: false,
    require: [
        'Ext.data.proxy.Memory',
        'Ext.data.reader.Json',
        'Ext.data.writer.Json',
        'Calendar.model.event.EventModel'
    ], 
    proxy: {
        type: 'baseproxy',
        //url : Routing.generate('desktop_cliente_list_by_empresa', null, true),
        //url: 'http://localhost/redbsa/web/app_dev.php/admin/event/list',
        api: {
            create  : Routing.generate('event_create_admin', {}, true),
            read    : Routing.generate('event_list_admin', {}, true),
            update  : null,
            destroy : null
        },


        reader: {
            type: 'json',
            root: 'data.entities',
            successProperty: 'success',
            totalProperty: 'data.total'
        },
        
        writer : {
            //type: 'BaseWriter',
            encode: false,
            root : 'event',
            nameProperty: 'mapping',
        }
        
    },

    // private
    constructor: function(config){
        this.callParent(arguments);
        
        this.sorters = this.sorters || [{
            property: 'StartDate',
            direction: 'ASC'
        }];
        
        this.idProperty = this.idProperty || 'id';
        this.fields = Calendar.model.event.EventModel.prototype.fields.getRange();
        this.onCreateRecords = Ext.Function.createInterceptor(this.onCreateRecords, this.interceptCreateRecords);
        this.initRecs();
    },
    
    // private - override to make sure that any records added in-memory
    // still get a unique PK assigned at the data level
    interceptCreateRecords: function(records, operation, success) {
        console.log(operation);
        var r = Ext.decode(operation.response.responseText);

        if (success) {
            
            var i = 0,
                rec,
                len = records.length;
            
            for (; i < len; i++) {
                console.log('r');
                console.log(records[i]);
                records[i].data['EventId'] = r.id;
                console.log(records[i]);
            }

        }
    },
    
    // If the store started with preloaded inline data, we have to make sure the records are set up
    // properly as valid "saved" records otherwise they may get "added" on initial edit.
    initRecs: function(){
        this.each(function(rec){
            rec.store = this;
            rec.phantom = false;
        }, this);
    },
    
    // private - override the default logic for memory storage
    onProxyLoad: function(operation) {
        var me = this,
            records;
        
        if (me.data && me.data.length > 0) {
            // this store has already been initially loaded, so do not reload
            // and lose updates to the store, just use store's latest data
            me.totalCount = me.data.length;
            records = me.data.items;
        }
        else {
            // this is the initial load, so defer to the proxy's result
            var resultSet = operation.getResultSet(),
                successful = operation.wasSuccessful();

            records = operation.getRecords();

            if (resultSet) {
                me.totalCount = resultSet.total;
            }
            if (successful) {
                me.loadRecords(records, operation);
            }
        }

        me.loading = false;
        me.fireEvent('load', me, records, successful);
    }

});
