Ext.define('Calendar.model.event.EventModel', {
    extend: 'Ext.data.Model',
    requires: [
        //'AppDesktop.model.empresa.EmpresaModel'
    ],
    fields: [
    {
        name: 'EventId',
        mapping: 'id',
        type: 'int'
    },
    {
        name: 'CalendarId',
        mapping: 'cid',
        type: 'int'
    },
    {
        name: 'Title',
        mapping: 'title',
        type: 'string'
    },
    {
        name: 'StartDate',
        mapping: 'start',
        type: 'date',
        dateFormat: 'c',
        dateWriteFormat: 'Y-m-d H:i:s'
    },
    {
        name: 'EndDate',
        mapping: 'end',
        type: 'date',
        dateFormat: 'c',
        dateWriteFormat: 'Y-m-d H:i:s'
    },
    {
        name: 'Location',
        mapping: 'loc',
        type: 'string'
    },
    {
        name: 'Notes',
        mapping: 'notes',
        type: 'string'
    },
    {
        name: 'Url',
        mapping: 'url',
        type: 'string'
    },
    {
        name: 'IsAllDay',
        mapping: 'ad',
        type: 'boolean'
    },
    {
        name: 'Reminder',
        mapping: 'rem',
        type: 'string'
    },
    {
        name: 'IsNew',
        mapping: 'n',
        type: 'boolean'
    }
    ],
    
    validations: [
    ]
    
    
    
    
   
});