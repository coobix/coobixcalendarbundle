Ext.define('AppDesktop.baseclass.proxy.BaseProxy', {
  extend: 'Ext.data.proxy.Ajax',
  alias: 'proxy.baseproxy',
  timeout: 60000,
  pageParam: '_page',
  config: {
    reader: 'json',
  },
  writer: {
    type: 'json'

  },
  encodeFilters: function(filters) {
    var length   = filters.length,
                   filtStrs = [],
                   filter, i;
 
    for (i = 0; i < length; i++) {
      filter = filters[i];
      if(filter.property.indexOf("_id") !== '-1') {
        var nombre = filter.property.split('_id');
        filter.property = nombre[0];
      }
    filtStrs[i] = filter.property + '=' + filter.value
    }
    return filtStrs.join("&");
  },
  
  buildRequest: function(operation) {
    var request = this.callParent(arguments);
    
    var filters = request.operation.filters;
    var params = request.params;
    if (filters && filters.length > 0) {
      delete params['filter'];
      for (var n = 0; n < filters.length; n++) {
        params[filters[n].property] = filters[n].value;
      }
    } 

    return request;
  },

  
});