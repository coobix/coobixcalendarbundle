Ext.define('AppDesktop.baseclass.controller.BaseModuleController', {
    extend: 'AppDesktop.baseclass.controller.BaseController',
    requires: [
        'AppDesktop.baseclass.view.BaseModuleView'
    ],
    moduleName: null,
    config: {
        empresa: null
    },
    init: function () {
        this.callParent();
    },
    open: function () {
        var me = this;

        if (me.isOpen()) {
            return true;
        }

        var components = Ext.ComponentQuery.query('DesktopTaskBarView');
        if (components.length > 1) {
            alert('Error! Hay mas de un desktopTaskBarView: Archivo BaseModuleController.js\n\
                    método showList');
        }
        var desktopTaskBarView = components[0];

        var moduleViewWin = Ext.create('AppDesktop.view.' + me.moduleName.toLowerCase() + '.' + me.moduleName + 'View');

        var win = desktopTaskBarView.createWindow(moduleViewWin);

        win.on('show', function () {
            me.empresaLogin();
        });
        win.show();

    },
    empresaLogin: function () {
        var me = this;

        if (me.getEmpresa()) {
            return;
        }

        var empresaLoginForm = Ext.create('AppDesktop.form.' + me.moduleName.toLowerCase() + '.' + me.moduleName + 'EmpresaLoginForm');
        var empresaLoginWin = Ext.create('Ext.window.Window', {
            layout: 'fit',
            title: 'Seleccionar Empresa',
            modal: true,
            closable: false,
            height: 120,
            width: 300,
            items: [empresaLoginForm]
        });

        empresaLoginWin.show();
    },
    getModuleView: function () {
        var components = Ext.ComponentQuery.query(this.moduleName + 'View');
        if (components.length > 1) {
            alert('Error! Hay mas de un moduleView: Archivo BaseModuleController.js\n\
                    método openIngresoComprobante');
        }
        var moduleView = components[0];

        return moduleView;
    },
    close: function (button) {
        var me = this;
        var moduleView = me.getModuleView();
        moduleView.close();
        button.up('window').close();
    },
    closeTab: function (cmp) {
        var me = this;
        var tabpanel = cmp.up('tabpanel');
        tabpanel.remove(tabpanel.getActiveTab());
    },
    isOpen: function () {
        var components = Ext.ComponentQuery.query(this.moduleName + 'View');
        return  (components.length > 0) ? true : false;
    },
    logIn: function (button) {
        var me = this;
        var formPanel = button.up('form');

        if (formPanel.isValid()) {
            var empresaField = formPanel.child('[name=empresa]');
            var id = empresaField.getValue();
            var empresa = empresaField.getStore().getById(id);

            var moduleView = me.getModuleView();
            moduleView.setEmpresa(empresa);
            moduleView.setTitle(this.moduleTitle + ': ' + empresa.get('razonSocial'));
            var win = formPanel.up('window');
            win.close();
        }

        return;
    },
    getEmpresa: function () {
        var me = this;
        var moduleView = me.getModuleView();
        return moduleView.getEmpresa();

    }
});