Ext.define('AppDesktop.baseclass.controller.BaseController', {
    extend: 'Ext.app.Controller',
    /* class name no deberia ser null deferia ser false u otro primitivo */
    className: null,
    classTitle: '',
    classTitlePlural: '',
    /*
     * #-CI
     * Esta propiedad se va a la clase de la ventana cuando se extienda
     */
    listTitle: '',
    createFlashMsg: 'Registro Creado.',
    updateFlashMsg: 'Registro Modificado.',
    deleteFlashMsg: 'Registro Eliminado.',
    
    showList: function () {

        var me = this;
        /*
         * #-CI
         * Esta ventana hay que pasarla a una clase para llamarla 
         * directamente
         * Asi la vas a poder hacer heredar y por ejemplo setearle el title
         * 
         */
        if (me.listTitle == '') {
            me.listTitle = 'Listado de ' + me.className + 's';
        }

        var store = Ext.create('AppDesktop.store.' + me.className.toLowerCase() + '.' + me.className + 'Store');
        store.load();
        var listView = Ext.create('AppDesktop.view.' + me.className.toLowerCase() + '.' + me.className + 'ListView', {
            store: store
        });
        var listWin = Ext.create('Ext.window.Window', {
            layout: 'fit',
            title: me.listTitle,
            items: [listView],
            width: '95%',
            height: '80%',
            maximizable: true,
            minimizable: true
        });

        //var app = AppDesktop.getApplication();
        //var desktop = app.getController('desktop.DesktopController');

        //var desktopTaskBarView = Ext.getCmp('desktopTaskBarView');
        var components = Ext.ComponentQuery.query('DesktopTaskBarView');
        if (components.length > 1) {
            alert('Error! Hay mas de un desktopTaskBarView: Archivo BaseController.js\n\
                    método showList');
        }
        var desktopTaskBarView = components[0];

        var win = desktopTaskBarView.createWindow(listWin);

        win.show();
    },
    newAction: function () {
        var me = this;
        var components = Ext.ComponentQuery.query('DesktopTaskBarView');
        if (components.length > 1) {
            alert('Error! Hay mas de un desktopTaskBarView: Archivo BaseController.js\n\
                    método showList');
        }
        var desktopTaskBarView = components[0];

        var newWin = Ext.create('AppDesktop.view.' + me.className.toLowerCase() + '.' + me.className + 'NewView');
        var win = desktopTaskBarView.createWindow(newWin);
        win.show();
    },
    createAction: function (button) {
        var me = this;
        console.log(me.className);
        var win = button.up('window');
        var form = win.down('form');
        var comprobanteRequest = {};
        Ext.Object.merge(comprobanteRequest, me.prepareFormSFRequest(form.getForm(), ["id"]));
        if (form.isValid()) { // make sure the form contains valid data before submitting
            Ext.Ajax.request({
                url: Routing.generate('desktop_' + me.className.toLowerCase() + '_create', null, true),
                params: comprobanteRequest,
                method: 'POST',
                success: function (response, opts) {
                    var r = Ext.decode(response.responseText);
                    if (r.success) {
                        win.close();
                        console.log(me.className + 'List');
                        var listGrids = Ext.ComponentQuery.query(me.className + 'List');
                        Ext.Array.each(listGrids, function (listGrid) {
                            listGrid.getStore().load();
                        });
                        var app = AppDesktop.getApplication();
                        var desktop = app.getController('AppDesktop.controller.desktop.DesktopController');
                        desktop.msg('success', '<b>' + me.createFlashMsg + '</b>');
                    }
                    else {
                        Ext.Msg.alert('Datos Inválidos', r.errors);
                    }
                    button.enable();
                },
                failure: function (response, opts) {
                    Ext.Msg.alert("Error en el servicio.", 'Ha ocurrido un error en el servicio, intentelo más tarde.');
                    button.enable();
                }
            });
        } else { // display error alert if the data is invalid
            Ext.Msg.alert('Datos Inválidos', 'Por favor corrija el formulario.');
            button.enable();
        }
    },
    editAction: function (grid, record) {
        var me = this;
        var listWin = grid.up('window');
        var components = Ext.ComponentQuery.query('DesktopTaskBarView');
        if (components.length > 1) {
            alert('Error! Hay mas de un desktopTaskBarView: Archivo BaseController.js\n\
                    método showList');
        }
        var desktopTaskBarView = components[0];

        var editWin = Ext.create('AppDesktop.view.' + me.className.toLowerCase() + '.' + me.className + 'EditView',
                {
                    'listWin': listWin
                }
        );
        editWin.down('form').loadRecord(record);
        var win = desktopTaskBarView.createWindow(editWin);
        win.show();
    },
    updateAction: function (button) {
        var me = this;
        var win = button.up('window');
        var form = win.down('form');
        //var record = form.getRecord();
        //var formValues = form.getValues();
        //record.set(formValues);
        var comprobanteRequest = {};
        Ext.Object.merge(comprobanteRequest, me.prepareFormSFRequest(form.getForm(), ['id']));
        var formValues = form.getForm().getFieldValues();
        var requestQuery = {'id': formValues['id']}
        if (form.isValid()) { // make sure the form contains valid data before submitting
            Ext.Ajax.request({
                url: Routing.generate('desktop_' + me.className.toLowerCase() + '_update', requestQuery, true),
                params: comprobanteRequest,
                method: 'POST',
                success: function (response, opts) {
                    var r = Ext.decode(response.responseText);
                    if (r.success) {

                        // #-CI
                        //*Esto se podria hacer con la propiedad action
                        // en vez de getText, fijarse
                        if (button.getText() !== "Aplicar") {
                            win.close();
                        }

                        // #-CI
                        // Deberias centralizar con eventos la actualizacion
                        // de las stores
                        if (win.listWin.down('grid') !== null) {
                            win.listWin.down('grid').getStore().load();
                        }

                        /*
                         var listGrids = Ext.ComponentQuery.query(me.className + 'List');
                         Ext.Array.each(listGrids, function (listGrid) {
                         listGrid.getStore().load();
                         });
                         */
                        var app = AppDesktop.getApplication();
                        var desktop = app.getController('AppDesktop.controller.desktop.DesktopController');
                        desktop.msg('success', '<b>' + me.updateFlashMsg + '</b>');
                    }
                    else {
                        Ext.Msg.alert('Datos Inválidos', r.errors);
                    }
                    button.enable();
                },
                failure: function (response, opts) {
                    Ext.Msg.alert("Ha ocurrido un error conectando con el servidor");
                    button.enable();
                }
            });

        } else { // display error alert if the data is invalid
            Ext.Msg.alert('Datos Inválidos', 'Por favor corrija el formulario.');
            button.enable();
        }

    },
    gridDelete: function (grid, rowIndex) {

        var me = this;
        var rec = grid.getStore().getAt(rowIndex);
        var recId = rec.get('id');
        Ext.MessageBox.confirm('Eliminar', 'Desea eliminar el registro?', function (btn) {
            var requestQuery = {'id': recId}
            var requestData = {};
            requestData['form_' + recId + '[id]'] = recId;
            if (btn == 'yes') {
                Ext.Ajax.request({
                    url: Routing.generate('desktop_' + me.className.toLowerCase() + '_delete', requestQuery, true),
                    params: requestData,
                    method: 'POST',
                    success: function (response, opts) {
                        var r = Ext.decode(response.responseText);
                        if (!r.success) {
                            Ext.Msg.alert('Error', r.errors);
                            return false;
                        }
                        grid.getStore().load();
                        var app = AppDesktop.getApplication();
                        var desktop = app.getController('AppDesktop.controller.desktop.DesktopController');
                        desktop.msg('info', '<b>' + me.deleteFlashMsg + '</b>');
                    },
                    failure: function (response, opts) {
                        Ext.Msg.alert("Ha ocurrido un error conectando con el servidor");
                        
                    }
                });
            }
        });

    },
    deleteAction: function (button) {
        var me = this;
        Ext.MessageBox.confirm('Eliminar', 'Desea eliminar el registro?', function (btn) {
            if (btn == 'yes') {
                var win = button.up('window');
                var form = win.down('form');
                var formValues = form.getValues();
                var requestData = {};
                requestData['form_' + formValues['id'] + '[id]'] = formValues['id'];
                Ext.Ajax.request({
                    url: Routing.generate('desktop_' + me.className.toLowerCase() + '_delete', {'id': formValues['id']}, true),
                    params: requestData,
                    method: 'POST',
                    success: function (response, opts) {
                        var r = Ext.decode(response.responseText);
                        if (!r.success) {
                            Ext.Msg.alert('Error', r.errors);
                            button.enable();
                            return false;
                        }
                        win.close();
                        if (win.listWin.down('grid') !== null) {
                            win.listWin.down('grid').getStore().load();
                        }
                        var app = AppDesktop.getApplication();
                        var desktop = app.getController('AppDesktop.controller.desktop.DesktopController');
                        desktop.msg('info', '<b>' + me.deleteFlashMsg + '</b>');
                        button.enable();
                    },
                    failure: function (response, opts) {
                        Ext.Msg.alert("Ha ocurrido un error conectando con el servidor");
                        button.enable();
                    }
                });
            }

        });


    },
    listBy: function (listByConfig) {
        var me = this;
        var storeName = 'AppDesktop.store.' + me.className.toLowerCase() + '.' + me.className + 'By' + listByConfig.listByClassName + 'Store';

        var store = Ext.create(storeName);
        var proxy = store.getProxy();
        proxy.extraParams[listByConfig.listByClassName.toLowerCase()] = 
            listByConfig.listByObject.getId()
        ;
        store.load();
        var listClassName = 'AppDesktop.view.' + me.className.toLowerCase() + '.' + me.className + 'By' + listByConfig.listByClassName + 'ListView';
        
        var list = Ext.create(listClassName, {
            store: store,
            listByObject: listByConfig.listByObject
        });
        var listWin = Ext.create('Ext.window.Window', {
            layout: 'fit',
            title: listByConfig.windowTitle,
            items: [list],
            width: 800,
            height: 325,
            maximizable: true,
            minimizable: true,
            listByObject: listByConfig.listByObject,
            listByClassName: listByConfig.listByClassName
        });
        var components = Ext.ComponentQuery.query('DesktopTaskBarView');
        if (components.length > 1) {
            alert('Error! Hay mas de un desktopTaskBarView: Archivo BaseController.js\n\
                    método showList');
        }
        var desktopTaskBarView = components[0];
        var win = desktopTaskBarView.createWindow(listWin);
        win.show();
    },
    newBy: function (button) {
        var me = this;
        var listWin = button.up('window');


        var form = Ext.create('AppDesktop.form.' + me.className.toLowerCase() + '.' + me.className + 'By' + listWin.listByClassName + 'NewForm');
        form.getForm().setValues({empresa: listWin.listByObject.data.id});
        //var empresaField = form.findField('empresa');
        var newByWin = Ext.create('AppDesktop.view.' + me.className.toLowerCase() + '.' + me.className + 'By' + listWin.listByClassName + 'NewView',
                {
                    'form': form,
                    'byClassName': listWin.listByClassName
                }
        );
        var components = Ext.ComponentQuery.query('DesktopTaskBarView');
        if (components.length > 1) {
            alert('Error! Hay mas de un desktopTaskBarView: Archivo BaseController.js\n\
                    método showList');
        }
        var desktopTaskBarView = components[0];
        var win = desktopTaskBarView.createWindow(newByWin);
        win.show();
    },
    createBy: function (button) {
        var me = this;
        var win = button.up('window');
        var byClassName = win.byClassName;

        var form = win.down('form');

        var formValues = form.getValues();
        var values = {};
        values[me.className.toLowerCase() + '[byClassName]'] = byClassName;
        Ext.Object.each(formValues, function (key, value) {
            values[me.className.toLowerCase() + '[' + key + ']'] = value;
        });

        if (form.isValid()) { // make sure the form contains valid data before submitting
            Ext.Ajax.request({
                url: form.action,
                params: values,
                method: 'POST',
                success: function (response, opts) {
                    var r = Ext.decode(response.responseText);
                    if (r.success) {
                        win.close();
                        //ClienteByEmpresaList
                        var listGrids = Ext.ComponentQuery.query(me.className + 'By' + byClassName + 'List');
                        Ext.Array.each(listGrids, function (listGrid) {
                            listGrid.getStore().load();
                        });
                        var app = AppDesktop.getApplication();
                        var desktop = app.getController('AppDesktop.controller.desktop.DesktopController');
                        desktop.msg('success', '<b>' + me.createFlashMsg + '</b>');
                    }
                    else {
                        Ext.Msg.alert('Datos Inválidos', r.errors);
                    }
                },
                failure: function (response, opts) {
                    Ext.Msg.alert("Error en el servicio.", 'Ha ocurrido un error en el servicio, intentelo más tarde.');
                }
            });
        } else { // display error alert if the data is invalid
            Ext.Msg.alert('Datos Inválidos', 'Por favor corrija el formulario.');
        }
    },
    open: function () {
        var me = this;
        me.showList();
    },
    prepareRecordSFRequest: function (records, childFormName, skip) {
        var me = this;
        var requestData = {};
        var i = 0;
        Ext.Array.each(records, function (item, index) {

            Ext.Object.each(item.getData(), function (key, value) {
                

                if (skip.indexOf(key) === -1) {
                    
                    if (value instanceof Date) {
                        var v = value.getDate() + '/' + (value.getMonth() + 1) + '/' + value.getFullYear();
                    }
                    else if (value instanceof Object) {
                        //console.log(typeof value)
                        //var v = value.get('id');
                        var v = value.id;
                    }
                    else {
                        var v = value;
                    }
                    requestData[me.className.toLowerCase() + '[' + childFormName + '][' + i + '][' + key + ']'] = v;
                }
            });
            i++;
        });

        return requestData;

    },
    prepareFormSFRequest: function (form, skip) {
        var me = this;
        var requestData = {};
        
        Ext.Object.each(form.getFieldValues(), function (key, value) {
            if (skip.indexOf(key) === -1) {
                if ((value instanceof Date)) {
                    requestData[me.className.toLowerCase() + '[' + key + ']'] = value.getDate() + '/' + (value.getMonth() + 1) + '/' + value.getFullYear();
                }
                else {
                    requestData[me.className.toLowerCase() + '[' + key + ']'] = value;
                }
            }

        });

        return requestData;
    },
    formatErrorResponse: function (errors) {
        var errorMsj = '';

        Ext.Object.each(errors, function (key, value) {
            errorMsj += value + '<br>';
        });
        return errorMsj;
    }
});