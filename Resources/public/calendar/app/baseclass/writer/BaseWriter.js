Ext.define('AppDesktop.baseclass.writer.BaseWriter', {
  extend: 'Ext.data.writer.Json',
  alias: 'writer.BaseWriter',
  encode: false,
  config: {
    className: null
  },
  getRecordData: function(record, operation) {
      var data = this.callParent(arguments);
      console.log(data);
      if (operation.action === 'create') {
        data = this.prepareRecordSFRequest(record, [])
      }
      console.log(data);
      return data;
    },

  prepareRecordSFRequest: function (record, skip) {
        var me = this;
        var requestData = {};
        
        Ext.Object.each(record.data, function (key, value) {
            if (skip.indexOf(key) === -1) {
                if ((value instanceof Date)) {
                    requestData[me.className.toLowerCase() + '[' + key + ']'] = value.getDate() + '/' + (value.getMonth() + 1) + '/' + value.getFullYear();
                }
                else {
                    requestData[me.className.toLowerCase() + '[' + key + ']'] = value;
                }
            }

        });

        return requestData;
    }
  
});