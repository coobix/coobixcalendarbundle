Ext.define('AppDesktop.baseclass.view.BaseEditView', {
    extend: 'Ext.window.Window',
    layout: 'fit',
    minimizable: true,
    autoShow: true,
    initComponent: function() {
        this.callParent();
    }
});