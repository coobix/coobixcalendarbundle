Ext.define('AppDesktop.baseclass.view.BaseNewView', {
    extend: 'Ext.window.Window',
    layout: 'fit',
    minimizable: true,
    //autoShow: true,
    initComponent: function () {
        this.callParent();
    }
});
