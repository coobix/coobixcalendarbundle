Ext.define('AppDesktop.baseclass.view.BaseModuleView', {
    extend: 'Ext.window.Window',
    requires: [],
    layout: 'border',
    title: '',
    floating: true,
    width: '95%',
    height: '93%',
    maximizable: true,
    minimizable: true,
    /*
     * no funciona
     * true to animate the transition when the panel is collapsed, 
     * false to skip the animation (defaults to true if the Ext.fx.Anim 
     * class is available, otherwise false). May also be specified as the 
     * animation duration in milliseconds.
     */
    animCollapse: true,
    /*
     * no funciona
     True to constrain the window header within its containing element 
     (allowing the window body to fall outside of its containing element) or 
     false to allow the header to fall outside its containing element. 
     Optionally the entire window can be constrained using constrain.
     */
    constrainHeader: false,
    /*
     * no funciona
     A shortcut to add or remove the border on the body of a panel. In the classic theme this only applies to a panel which has the frame configuration set to true.
     */
    bodyBorder: true,
    templateInicio: '',
    config: {
        empresa: null
    }
});