Ext.define('AppDesktop.baseclass.view.BaseListView', {
    extend: 'Ext.grid.Panel',
    requires: [
        'Ext.grid.column.RowNumberer',
        'Ext.grid.column.Boolean',
        'Ext.toolbar.Paging'
    ],
    layout: {
        type: 'fit'
    },
    initComponent: function () {
        this.callParent();
    },
    addStdColumns: function () {
        Ext.Array.insert(this.columns, 0, [{
                xtype: 'rownumberer'
            }]);
        Ext.Array.push(this.columns, {
            menuDisabled: true,
            sortable: false,
            xtype: 'actioncolumn',
            width: 40,
            items: [{
                    iconCls: 'deleteIcon',
                    tooltip: 'Eliminar',
                    handler: function (grid, rowIndex, colIndex) {
                        this.fireEvent('delete', grid, rowIndex);
                    }
                },
                {
                    iconCls: 'editIcon',
                    tooltip: 'Editar',
                    handler: function (grid, rowIndex, colIndex) {
                        this.fireEvent('edit', grid, rowIndex);
                    }

                }]
        });
    }
});
