Ext.define('AppDesktop.baseclass.form.BaseNewForm', {
    extend: 'Ext.form.Panel',
    bodyPadding: 10,
    defaultType: 'textfield',
    items:[],
    initComponent: function () {
        this.items = [];
        this.buildFormFields();
        this.buttons = this.buildButtons();
        this.callParent();
    },
    setDefaulValues: function () {
        return true;
    },
    buildButtons: function () {
        return	[
            {
                text: 'Crear',
                action: 'create',
                listeners: {
                    click: function () {
                        this.disable();
                    }
                }
            },
            {
                text: 'Cancelar',
                scope: this,
                handler: function () {
                    var win = this.up('window');
                    win.close();
                }
            }
        ];
    },
    buildFormFields: function () {}

});