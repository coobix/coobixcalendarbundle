Ext.define('AppDesktop.baseclass.form.EmpresaLoginForm', {
    extend: 'Ext.form.Panel',
    require: [
        'AppDesktop.store.empresa.EmpresaStore'
    ],
    alias: 'widget.EmpresaLoginForm',
    bodyPadding: 10,
    width: 200,
    defaultType: 'textfield',
    initComponent: function() {

        this.items = this.buildFormFields();
        this.buttons = this.buildButtons();
        this.callParent();
    },
    getEmpresaStore: function() {
        var store = Ext.create('AppDesktop.store.empresa.EmpresaStore');
        store.load();
        return store;
    },
    buildButtons: function() {
        var me = this;
        return	[
            {
                text: 'Entrar',
                action: 'login'
                
            },
            {
                text: 'Cancelar',
                action: 'cancelar'
            }
        ];
    },
    formFields: function() {
        return [
            {
                fieldLabel: 'Empresa',
                name: 'empresa',
                xtype: 'combobox',
                displayField: 'razonSocial',
                valueField: 'id',
                store: this.getEmpresaStore(),
                queryMode: 'local',
                allowBlank: false,
                forceSelection: true
                /*
                NO FUNCIONO, NO TE DEVUELVE EL VALOR DEL STORE
                listeners: {
                    afterrender:{
                        fn: function(combo) {
                            var recordSelected = combo.getStore().first();
                            combo.setValue(recordSelected.get('razonSocial'));
                        },
                        scope: this   
                    }
                }
                */

                    
            }   
        ];
    },
    buildFormFields: function() {
        return this.formFields();
    }

});