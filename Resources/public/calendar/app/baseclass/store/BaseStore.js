Ext.define('AppDesktop.baseclass.store.BaseStore', {
    extend: 'Ext.data.Store',
    autoLoad: false,
    pageSize: 10,
    
    listeners: {
        
        load: function (store, records, success, opts) {
            var responseRawData = store.getProxy().getReader().rawData;
            if (typeof responseRawData !== "undefined") {
                if(responseRawData.errors) {
                Ext.Msg.alert('Error', responseRawData.errors);
                }
            }
            else {
                Ext.Msg.alert('Error', 'Ocurrió un error, intente luego.');
            }
            
        }
    } 

    /*
    #-CI Deberias traer esta información aca y dejar solo las urls en
    los proxie hijos. No te funciona es como si todos los store utilizaran
    el mismo proxy
    */
    /*
    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'data.entities',
            successProperty: 'success',
            totalProperty: 'data.total'
        }
    }
    */
    
});
