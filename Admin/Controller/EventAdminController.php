<?php

namespace Coobix\CalendarBundle\Admin\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Coobix\AdminBundle\Controller\Sf2AdminController as BaseController;
use Coobix\AdminBundle\Entity\BaseList;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Event Admin controller.
 *
 */
class EventAdminController extends BaseController {

    public function listAction(Request $request, $class) {

        $admin = $this->get("coobix.admin.event");

        $em = $this->getDoctrine()->getManager();

        //CONSULTA PARA RETORNAR LAS ENTIDADES DE LA PAGINA
        //A ESTA CONSULTA LUEGO SE LE AGREGAN FILTROS DE ORDEN, PAGINADO, ETC
        $qb = $em->createQueryBuilder();
        $qb->select('e')->from($admin->getEntityShortcutName(), 'e');
        $qb->orderBy('e.createdAt', 'DESC');

        $list = new BaseList($this->getDoctrine(), $admin->getClass());
        $list->setQb($qb);

        $listSearchForm = $this->createSearchListForm($admin);
        $listSearchForm->handleRequest($request);
        $listSearchFormIsSubmited = false;
        if ($listSearchForm->isSubmitted()) {
            $listSearchFormIsSubmited = true;
            if ($listSearchForm->isValid()) {
                $list->applyFilters();
            }
        }

        $list->applyOrder();


        //PAGINADOR
        //Clono el query builder para el paginador
        $qbp = clone($list->getQb());
        $qbp->select('count(e.id)');
        $entitiesCount = $qbp->getQuery()->getSingleResult();

        $pager = $this->get('coobix.pager');
        //Url en la cual trabaja el paginador
        $listUrlParams = array(
            'routeName' => 'admin_list',
            'parameters' => array('class' => strtolower($admin->getClassName()))
        );
        //LA URL DEBERIA SER PASADA DIRECTAMENTE Y NO PARA QUE LA HAGA
        //EL PAGINADOR
        $pager->setListUrlParams($listUrlParams);
        $pager->setNumItems($entitiesCount[1]);
        $pager->configure();

        $list->applyLimits();
        $list->getResult();
        $entities = $list->getEntities();

        $rsp = [
            'success' => true,
            'error' => null,
            'data' => [
                'entities' => $entities,
                'total' => $entitiesCount[1],
                ]
            ]; 

            $serializer = $this->get('jms_serializer');
            $responseData = $serializer->serialize(
                    $rsp
                    , 'json');
            return new Response($responseData, 200, array('Content-Type' => 'application/json'));

    }


    public function createAction(Request $request, $class) {
        $admin = $this->get('coobix.admin.event');
        
        $data = json_decode($request->getContent(), true);
        $request->request->replace($data);

        $c = $admin->getClass();
        $entity = new $c;
        

        $form = $this->createCreateForm($admin, $entity);

        $form->handleRequest($request);
        
        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            //LOG
            $rsp = array(
                'success' => true,
                'errors' => null,
                'data' => [
                    'entities' => $entity,
                    'total' => 1,
                ]
            );
            $serializer = $this->get('jms_serializer');
            $responseData = $serializer->serialize(
                    $rsp
                    , 'json');
            return new Response($responseData, 200, array('Content-Type' => 'application/json'));

        } else {
            $responseData = array(
                'success' => false,
                'errors' => [0=> 'Existen errores en el formulario, intente nuevamente.'],
                'data' => null
            );
        }
       
        $response = new JsonResponse();
        $response->setData($responseData);
        return $response;
        
    }


    public function updateAction(Request $request, $class, $id) {

        $admin = $this->get('coobix.admin.' . $class);
        
        $data = json_decode($request->getContent(), true);
        $request->request->replace($data);

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($admin->getEntityShortcutName())->find($id);

        if (!$entity) {
            $responseData = array(
                'success' => false,
                'errors' => ['No se pudo encontrar el registro.'],
                'data' => null
            );

            $response = new JsonResponse();
            $response->setData($responseData);
            return $response;
        }

        $form = $this->createEditForm($admin, $entity);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $entity->setUpdatedAt(new \DateTime);
            $em->persist($entity);
            $em->flush();
            $this->get('coobix.log')->create($admin->getClassName() . ' actualiza/o: ' . $entity);

            $responseData = array(
                'success' => true,
                'errors' => null,
                'data' => null
            );
        } else {

            $responseData = array(
                'success' => false,
                'errors' =>  [0=> 'Existen errores en el formulario, intente nuevamente.'],
                'data' => null
            );
        }

        $response = new JsonResponse();
        $response->setData($responseData);
        return $response;
    }


    /**
     * Deletes a class.
     */
    public function deleteAction(Request $request, $id, $class) {
        
        $admin = $this->get("coobix.admin.event");

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository($admin->getEntityShortcutName())->find($id);

        $response = new JsonResponse();
        if (!$entity) {
            $responseData = array(
                'success' => false,
                'errors' => ['No se pudo encontrar el registro.'],
                'data' => null
            );

            $response->setData($responseData);
            return $response;
        }

        try {
                $em->remove($entity);
                $em->flush();
            } catch (\Exception $e) {
                $responseData = array('success' => false,
                    'errors' => ['No se pudo eliminar el registro por que tiene datos asociados.'],
                    'data' => null
                );

                $response->setData($responseData);
                return $response;
            }

            $this->get('coobix.log')->create($admin->getClassName() . ' eliminada/o: ' . $entity);
            $responseData = array('success' => true,
                'errors' => null,
                'data' => null
            );
        


        $response->setData($responseData);
        return $response;
    }


    protected function createDeleteForm($admin, $formOptions = array()) {

        $fo = array(
            'method' => 'DELETE',
            'id' => 0,
            'label' => ' ',
            'action' => null,
        );

        $o = array_merge($fo, $formOptions);

        if ($o['action'] === null) {
            $o['action'] = $this->generateUrl("admin_delete", array(
                'class' => strtolower($admin->getClassName()),
                'id' => $o['id'],
            ));
        }


        return $this->get('form.factory')->createNamedBuilder('form_' . $o['id'], 'form', null)
                        ->setAction($o['action'])
                        ->setMethod($o['method'])
                        ->add('id', 'hidden')
                        ->add('submit', 'submit', array('label' => $o['label']))
                        ->getForm();
    }


//methods
}
