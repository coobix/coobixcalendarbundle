<?php

namespace Coobix\CalendarBundle\Admin\Entity;

use Coobix\AdminBundle\Entity\Admin;
use Coobix\CalendarBundle\Form\EventType as NewType;
use Coobix\CalendarBundle\Form\EventEditType as EditType;
use Coobix\CalendarBundle\Admin\Form\EventListSearchType as ListSearchType;

class EventAdmin extends Admin {

    public function getNewTitle() {
        return "Crear nuevo Evento";
    }

    public function getEditTitle() {
        return "Editar Evento";
    }
    public function __construct($class) {
        $this->setNewForm(new NewType());
        $this->setEditForm(new EditType());
        $this->setListSearchForm(new ListSearchType());
        
        parent::__construct($class);
    }

}
