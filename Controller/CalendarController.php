<?php

namespace Coobix\CalendarBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CalendarController extends Controller
{
    
    public function showAction()
    {
        return $this->render('CoobixCalendarBundle:Calendar:show.html.twig');
    }
}
