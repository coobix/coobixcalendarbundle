<?php

namespace REDBSA\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Socio
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="REDBSA\AppBundle\Entity\SocioRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("codigo")
 */
class Socio {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="codigo", type="integer", unique=true)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var \Cuenta
     * @ORM\OneToOne(targetEntity="Cuenta", cascade={"persist", "remove"})
     */
    private $cuenta;
    
    /**
     * @var \Beneficio
     * 
     * @ORM\OneToMany(targetEntity="Beneficio", mappedBy="socio")
     **/
    private $beneficios;
    
    /**
     * @var \Cupon
     * 
     * @ORM\OneToMany(targetEntity="Cupon", mappedBy="socio")
     **/
    private $cupones;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\PrePersist
     */
    public function prePersist() {
        $this->setCreatedAt(new \DateTime);
        $this->setUpdatedAt(new \DateTime);
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate() {
        $this->setUpdatedAt(new \DateTime);
    }

    public function __toString() {
        return $this->nombre;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param integer $codigo
     * @return Socio
     */
    public function setCodigo($codigo) {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return integer 
     */
    public function getCodigo() {
        return $this->codigo;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Socio
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Socio
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Socio
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set cuenta
     *
     * @param \REDBSA\AppBundle\Entity\Cuenta $cuenta
     * @return Socio
     */
    public function setCuenta(\REDBSA\AppBundle\Entity\Cuenta $cuenta = null) {
        $this->cuenta = $cuenta;

        return $this;
    }

    /**
     * Get cuenta
     *
     * @return \REDBSA\AppBundle\Entity\Cuenta 
     */
    public function getCuenta() {
        return $this->cuenta;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->beneficios = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add beneficios
     *
     * @param \REDBSA\AppBundle\Entity\Beneficio $beneficios
     * @return Socio
     */
    public function addBeneficio(\REDBSA\AppBundle\Entity\Beneficio $beneficios)
    {
        $this->beneficios[] = $beneficios;

        return $this;
    }

    /**
     * Remove beneficios
     *
     * @param \REDBSA\AppBundle\Entity\Beneficio $beneficios
     */
    public function removeBeneficio(\REDBSA\AppBundle\Entity\Beneficio $beneficios)
    {
        $this->beneficios->removeElement($beneficios);
    }

    /**
     * Get beneficios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBeneficios()
    {
        return $this->beneficios;
    }

    /**
     * Add cupones
     *
     * @param \REDBSA\AppBundle\Entity\Cupon $cupones
     * @return Socio
     */
    public function addCupone(\REDBSA\AppBundle\Entity\Cupon $cupones)
    {
        $this->cupones[] = $cupones;

        return $this;
    }

    /**
     * Remove cupones
     *
     * @param \REDBSA\AppBundle\Entity\Cupon $cupones
     */
    public function removeCupone(\REDBSA\AppBundle\Entity\Cupon $cupones)
    {
        $this->cupones->removeElement($cupones);
    }

    /**
     * Get cupones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCupones()
    {
        return $this->cupones;
    }
}
