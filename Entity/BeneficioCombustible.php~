<?php

namespace REDBSA\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use REDBSA\AppBundle\Entity\Beneficio;
use JMS\Serializer\Annotation as JMS;

/**
 * Beneficio
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class BeneficioCombustible extends Beneficio
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \Combustible
     * 
     * @ORM\ManyToOne(targetEntity="Combustible")
     * @Assert\NotNull()
     * @JMS\Exclude
     * 
     */
    protected $combustible;

    /**
     * @var \Surtidor
     * 
     * @ORM\ManyToOne(targetEntity="Surtidor")
     * @Assert\NotNull()
     * @JMS\Exclude
     * */
    protected $surtidor;

    /**
     * @var float
     * 
     * @ORM\Column(name="cantidad", type="float")
     * @Assert\GreaterThan(value = 0)
     * @Assert\Type(type="float")
     * 
     * */
    protected $cantidad;

    /**
     * @var float
     * 
     * @ORM\Column(name="precio", type="float")
     * 
     * */
    protected $precio;

    /**
     * @var \CuponBeneficioCombustible
     * 
     * @ORM\OneToOne(targetEntity="CuponBeneficioCombustible", mappedBy="beneficio")
     * */
    protected $cupon;


    public function getDescuento($porcentajeDescuento = 0.03) {
        $val = $this->getImporte() * $porcentajeDescuento;
        return round($val, 2, PHP_ROUND_HALF_UP);
    }

    public function getImporte() {
        $val = ($this->precio * $this->cantidad);
        return round($val, 2, PHP_ROUND_HALF_UP);
    }

    public function getImporteNeto() {
        $val = $this->getImporte() - $this->getDescuento();
        return round($val, 2, PHP_ROUND_HALF_UP);
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist() {
        parent::prePersist();
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate() {
        parent::preUpdate();
    }



    /**
     * Set cantidad
     *
     * @param float $cantidad
     * @return BeneficioCombustible
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return float 
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set precio
     *
     * @param float $precio
     * @return BeneficioCombustible
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return float 
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set combustible
     *
     * @param \REDBSA\AppBundle\Entity\Combustible $combustible
     * @return BeneficioCombustible
     */
    public function setCombustible(\REDBSA\AppBundle\Entity\Combustible $combustible = null)
    {
        $this->combustible = $combustible;

        return $this;
    }

    /**
     * Get combustible
     *
     * @return \REDBSA\AppBundle\Entity\Combustible 
     */
    public function getCombustible()
    {
        return $this->combustible;
    }

    /**
     * Set surtidor
     *
     * @param \REDBSA\AppBundle\Entity\Surtidor $surtidor
     * @return BeneficioCombustible
     */
    public function setSurtidor(\REDBSA\AppBundle\Entity\Surtidor $surtidor = null)
    {
        $this->surtidor = $surtidor;

        return $this;
    }

    /**
     * Get surtidor
     *
     * @return \REDBSA\AppBundle\Entity\Surtidor 
     */
    public function getSurtidor()
    {
        return $this->surtidor;
    }

    /**
     * Set cupon
     *
     * @param \REDBSA\AppBundle\Entity\CuponBeneficioCombustible $cupon
     * @return BeneficioCombustible
     */
    public function setCupon(\REDBSA\AppBundle\Entity\CuponBeneficioCombustible $cupon = null)
    {
        $this->cupon = $cupon;

        return $this;
    }

    /**
     * Get cupon
     *
     * @return \REDBSA\AppBundle\Entity\CuponBeneficioCombustible 
     */
    public function getCupon()
    {
        return $this->cupon;
    }
}
