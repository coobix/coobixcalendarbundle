<?php

namespace Coobix\CalendarBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * Event
 *
 * @ORM\Table()
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */

class Event
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="EventId", type="integer", nullable=true)
     */
    private $EventId;

    /**
     * @var integer
     *
     * @ORM\Column(name="cid", type="integer", nullable=true)
     */
    private $cid;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100, nullable=true)
     * @Assert\Length(min = 2, max = 100)
     * @Assert\Type(type="string")
     */
    private $title;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime", nullable=true)
     * 
     * @Assert\DateTime()
     */
    private $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="datetime", nullable=true)
     * 
     * @Assert\DateTime()
     */
    private $end;

    /**
     * @var string
     *
     * @ORM\Column(name="loc", type="string", length=100, nullable=true)
     * @Assert\Length(min = 2, max = 100)
     * @Assert\Type(type="string")
     */
    private $loc;

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="string", length=100, nullable=true)
     * @Assert\Length(min = 2, max = 100)
     * @Assert\Type(type="string")
     */
    private $notes;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=100, nullable=true)
     * @Assert\Length(min = 2, max = 100)
     * @Assert\Type(type="string")
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="ad", type="string", length=100, nullable=true)
     * @Assert\Type(type="string")
     */
    private $ad;

    /**
     * @var string
     *
     * @ORM\Column(name="rem", type="string", length=100, nullable=true)
     * @Assert\Length(min = 2, max = 100)
     * @Assert\Type(type="string")
     */
    private $rem;

    /**
     * @var string
     *
     * @ORM\Column(name="n", type="string", length=100, nullable=true)
     * @Assert\Type(type="string")
     */
    private $n;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     * @Assert\DateTime()
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime")
     * @Assert\DateTime()
     */
    private $updatedAt;

   /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->setCreatedAt (new \DateTime);
        $this->setUpdatedAt (new \DateTime);
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->setUpdatedAt (new \DateTime);
    }

    public function __toString() {
        return $this->title;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set EventId
     *
     * @param integer $eventId
     * @return Event
     */
    public function setEventId($eventId)
    {
        $this->EventId = $eventId;

        return $this;
    }

    /**
     * Get EventId
     *
     * @return integer 
     */
    public function getEventId()
    {
        return $this->EventId;
    }

    /**
     * Set cid
     *
     * @param integer $cid
     * @return Event
     */
    public function setCid($cid)
    {
        $this->cid = $cid;

        return $this;
    }

    /**
     * Get cid
     *
     * @return integer 
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Event
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return Event
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     * @return Event
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime 
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set loc
     *
     * @param string $loc
     * @return Event
     */
    public function setLoc($loc)
    {
        $this->loc = $loc;

        return $this;
    }

    /**
     * Get loc
     *
     * @return string 
     */
    public function getLoc()
    {
        return $this->loc;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return Event
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Event
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set ad
     *
     * @param string $ad
     * @return Event
     */
    public function setAd($ad)
    {
        $this->ad = $ad;

        return $this;
    }

    /**
     * Get ad
     *
     * @return string 
     */
    public function getAd()
    {
        return $this->ad;
    }

    /**
     * Set rem
     *
     * @param string $rem
     * @return Event
     */
    public function setRem($rem)
    {
        $this->rem = $rem;

        return $this;
    }

    /**
     * Get rem
     *
     * @return string 
     */
    public function getRem()
    {
        return $this->rem;
    }

    /**
     * Set n
     *
     * @param boolean $n
     * @return Event
     */
    public function setN($n)
    {
        $this->n = $n;

        return $this;
    }

    /**
     * Get n
     *
     * @return boolean 
     */
    public function getN()
    {
        return $this->n;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Event
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Event
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
