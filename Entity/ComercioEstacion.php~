<?php

namespace REDBSA\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use REDBSA\AppBundle\Entity\Comercio;

/**
 * ComercioEstacion
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="REDBSA\AppBundle\Entity\ComercioRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ComercioEstacion extends Comercio
{

    /**
     * @var \Combustibles
     * 
     * @ORM\OneToMany(targetEntity="Combustible", mappedBy="comercio", cascade={"persist", "remove"})
     * @Assert\Collection()
     * */
    private $combustibles;

    /**
     * @var \Surtidor
     * 
     * @ORM\OneToMany(targetEntity="Surtidor", mappedBy="comercio", cascade={"persist", "remove"})
     * @Assert\Collection()
     * */
    private $surtidores;

    /**
     * @ORM\PrePersist
     */
    public function prePersist() {
        parent::prePersist();
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate() {
        parent::preUpdate();
    }
    
    /**
     * Add combustibles
     *
     * @param \REDBSA\AppBundle\Entity\Combustible $combustibles
     * @return ComercioEstacion
     */
    public function addCombustible(\REDBSA\AppBundle\Entity\Combustible $combustibles) {
        $combustibles->setComercio($this);

        $this->combustibles->add($combustibles);

        return $this;
    }
    
    /**
     * Add surtidores
     *
     * @param \REDBSA\AppBundle\Entity\Surtidor $surtidores
     * @return ComercioEstacion
     */
    public function addSurtidore(\REDBSA\AppBundle\Entity\Surtidor $surtidores) {

        $surtidores->setComercio($this);

        $this->surtidores->add($surtidores);

        return $this;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->combustibles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->surtidores = new \Doctrine\Common\Collections\ArrayCollection();
    }

    
    /**
     * Remove combustibles
     *
     * @param \REDBSA\AppBundle\Entity\Combustible $combustibles
     */
    public function removeCombustible(\REDBSA\AppBundle\Entity\Combustible $combustibles) {
        $this->combustibles->removeElement($combustibles);
    }

    /**
     * Get combustibles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCombustibles() {
        return $this->combustibles;
    }

    

    /**
     * Remove surtidores
     *
     * @param \REDBSA\AppBundle\Entity\Surtidor $surtidores
     */
    public function removeSurtidore(\REDBSA\AppBundle\Entity\Surtidor $surtidores) {
        $this->surtidores->removeElement($surtidores);
    }

    /**
     * Get surtidores
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSurtidores() {
        return $this->surtidores;
    }

}
