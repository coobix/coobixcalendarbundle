<?php

namespace REDBSA\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * Beneficio
 * @ORM\Entity(repositoryClass="REDBSA\AppBundle\Entity\BeneficioRepository")
 * @ORM\Table
 * @ORM\HasLifecycleCallbacks()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"beneficiocomercio" = "BeneficioComercio", "beneficiocombustible" = "BeneficioCombustible"})
 */
class Beneficio 
{
    /**
     * @JMS\Exclude
     */
    public static $nombreTipo = array(
        'beneficiocomercio' => 'Beneficio Comercio',
        'beneficiocombustible' => 'Beneficio Combustible'
    );

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100)
     * @Assert\NotBlank()
     * @Assert\Length(min = 2, max = 100)
     * @Assert\Type(type="string")
     */
    private $nombre;
    
    /**
     * @var \Comercio
     * 
     * @ORM\ManyToOne(targetEntity="Comercio", inversedBy="beneficios")
     * @Assert\NotNull()
     * @JMS\Exclude
     * 
     */
    private $comercio;

    /**
     * @var \Socio
     * 
     * @ORM\ManyToOne(targetEntity="Socio", inversedBy="beneficios")
     * @Assert\NotNull()
     * @JMS\Exclude
     * 
     */
    private $socio;

    /**
     * @var \REDBSA\UserBundle\Entity\User
     * 
     * @ORM\ManyToOne(targetEntity="REDBSA\UserBundle\Entity\User")
     * @JMS\Exclude
     * 
     * */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     * @Assert\DateTime()
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime")
     * @Assert\DateTime()
     * @JMS\Exclude
     */
    private $updatedAt;

    /**
     * @ORM\PrePersist
     */
    public function prePersist() {
        $this->setCreatedAt(new \DateTime);
        $this->setUpdatedAt(new \DateTime);
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate() {
        $this->setUpdatedAt(new \DateTime);
    }

    public function __toString() {
        return $this->nombre;
    }

    public function getNombreTipo() {
        return self::$nombreTipo[$this->tipo];
    }

    public function getDiscr() {
        $type = explode('\\', get_class($this));

        return end($type);
    }

    
   

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Beneficio
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Beneficio
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Beneficio
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Beneficio
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set comercio
     *
     * @param \REDBSA\AppBundle\Entity\Comercio $comercio
     * @return Beneficio
     */
    public function setComercio(\REDBSA\AppBundle\Entity\Comercio $comercio = null)
    {
        $this->comercio = $comercio;

        return $this;
    }

    /**
     * Get comercio
     *
     * @return \REDBSA\AppBundle\Entity\Comercio 
     */
    public function getComercio()
    {
        return $this->comercio;
    }

    /**
     * Set socio
     *
     * @param \REDBSA\AppBundle\Entity\Socio $socio
     * @return Beneficio
     */
    public function setSocio(\REDBSA\AppBundle\Entity\Socio $socio = null)
    {
        $this->socio = $socio;

        return $this;
    }

    /**
     * Get socio
     *
     * @return \REDBSA\AppBundle\Entity\Socio 
     */
    public function getSocio()
    {
        return $this->socio;
    }

    /**
     * Set user
     *
     * @param \REDBSA\UserBundle\Entity\User $user
     * @return Beneficio
     */
    public function setUser(\REDBSA\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \REDBSA\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
