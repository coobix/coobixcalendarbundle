<?php

namespace REDBSA\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cuenta
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="REDBSA\AppBundle\Entity\CuentaRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Cuenta
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="saldo", type="float")
     */
    private $saldo;
    
    /**
     * @var float
     *
     * @ORM\Column(name="cuota", type="float", nullable=true)
     */
    private $cuota;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaUltimoProceso", type="date", nullable=true)
     */
    private $fechaUltimoProceso;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaUltimoDebito", type="date", nullable=true)
     */
    private $fechaUltimoDebito;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaUltimoCredito", type="date", nullable=true)
     */
    private $fechaUltimoCredito;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaBaja", type="date", nullable=true)
     */
    private $fechaBaja;

    /**
     * @var string
     *
     * @ORM\Column(name="motivoBaja", type="string", length=255)
     */
    private $motivoBaja;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime")
     */
    private $updatedAt;

   /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
    	$this->setCreatedAt (new \DateTime);
    	$this->setUpdatedAt (new \DateTime);
    }
    
    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
    	$this->setUpdatedAt (new \DateTime);
    }

    public function __toString() {
        return '';
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set saldo
     *
     * @param float $saldo
     * @return Cuenta
     */
    public function setSaldo($saldo)
    {
        $this->saldo = $saldo;

        return $this;
    }

    /**
     * Get saldo
     *
     * @return float 
     */
    public function getSaldo()
    {
        return $this->saldo;
    }

    /**
     * Set fechaUltimoProceso
     *
     * @param \DateTime $fechaUltimoProceso
     * @return Cuenta
     */
    public function setFechaUltimoProceso($fechaUltimoProceso)
    {
        $this->fechaUltimoProceso = $fechaUltimoProceso;

        return $this;
    }

    /**
     * Get fechaUltimoProceso
     *
     * @return \DateTime 
     */
    public function getFechaUltimoProceso()
    {
        return $this->fechaUltimoProceso;
    }

    /**
     * Set fechaUltimoDebito
     *
     * @param \DateTime $fechaUltimoDebito
     * @return Cuenta
     */
    public function setFechaUltimoDebito($fechaUltimoDebito)
    {
        $this->fechaUltimoDebito = $fechaUltimoDebito;

        return $this;
    }

    /**
     * Get fechaUltimoDebito
     *
     * @return \DateTime 
     */
    public function getFechaUltimoDebito()
    {
        return $this->fechaUltimoDebito;
    }

    /**
     * Set fechaUltimoCredito
     *
     * @param \DateTime $fechaUltimoCredito
     * @return Cuenta
     */
    public function setFechaUltimoCredito($fechaUltimoCredito)
    {
        $this->fechaUltimoCredito = $fechaUltimoCredito;

        return $this;
    }

    /**
     * Get fechaUltimoCredito
     *
     * @return \DateTime 
     */
    public function getFechaUltimoCredito()
    {
        return $this->fechaUltimoCredito;
    }

    /**
     * Set fechaBaja
     *
     * @param \DateTime $fechaBaja
     * @return Cuenta
     */
    public function setFechaBaja($fechaBaja)
    {
        $this->fechaBaja = $fechaBaja;

        return $this;
    }

    /**
     * Get fechaBaja
     *
     * @return \DateTime 
     */
    public function getFechaBaja()
    {
        return $this->fechaBaja;
    }

    /**
     * Set motivoBaja
     *
     * @param string $motivoBaja
     * @return Cuenta
     */
    public function setMotivoBaja($motivoBaja)
    {
        $this->motivoBaja = $motivoBaja;

        return $this;
    }

    /**
     * Get motivoBaja
     *
     * @return string 
     */
    public function getMotivoBaja()
    {
        return $this->motivoBaja;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Cuenta
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Cuenta
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set cuota
     *
     * @param float $cuota
     * @return Cuenta
     */
    public function setCuota($cuota)
    {
        $this->cuota = $cuota;

        return $this;
    }

    /**
     * Get cuota
     *
     * @return float 
     */
    public function getCuota()
    {
        return $this->cuota;
    }
}
