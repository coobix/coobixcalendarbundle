<?php

namespace REDBSA\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use REDBSA\AppBundle\Entity\Comercio\ComercioBase;

/**
 * Comercio
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="REDBSA\AppBundle\Entity\ComercioRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"comercio" = "Comercio", "comercioestacion" = "ComercioEstacion"})
 */

class Comercio extends ComercioBase {
    
    /**
     * @var \Beneficio
     * 
     * @ORM\OneToMany(targetEntity="Beneficio", mappedBy="comercio")
     **/
    private $beneficios;
    
    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        parent::prePersist();
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        parent::preUpdate();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->beneficios = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add beneficios
     *
     * @param \REDBSA\AppBundle\Entity\Beneficio $beneficios
     * @return Comercio
     */
    public function addBeneficio(\REDBSA\AppBundle\Entity\Beneficio $beneficios)
    {
        $this->beneficios[] = $beneficios;

        return $this;
    }

    /**
     * Remove beneficios
     *
     * @param \REDBSA\AppBundle\Entity\Beneficio $beneficios
     */
    public function removeBeneficio(\REDBSA\AppBundle\Entity\Beneficio $beneficios)
    {
        $this->beneficios->removeElement($beneficios);
    }

    /**
     * Get beneficios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBeneficios()
    {
        return $this->beneficios;
    }
}
